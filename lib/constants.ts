/**
 * Copyright 2020 Vercel Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const SITE_URL = 'https://virtual-event-starter-kit-wine-one.vercel.app/';
export const SITE_ORIGIN = process.env.NEXT_PUBLIC_SITE_ORIGIN || new URL(SITE_URL).origin;
export const TWITTER_USER_NAME = 'focus';
export const BRAND_NAME = 'SEEK22';
export const SITE_NAME_MULTILINE = ['SEEK22', 'Conference'];
export const SITE_NAME = 'SEEK22';
export const META_DESCRIPTION =
  'This is an open source demo that Next.js developers can clone, deploy, and fully customize for events. Created through collaboration of marketers, designers, and developers at Vercel.';
export const SITE_DESCRIPTION =
  'This Dec. 30 – Jan. 3, SEEK22 will be in Salt Lake City! Come be part of an incredible five-day event where we bring to God all that we are — all our questions, desires, needs, hopes — so that He can make all that is within us new.';
export const DATE = 'December 30th - January 3rd';
export const SHORT_DATE = 'Dec 30th - Jan 3rd';
export const FULL_DATE = 'December 30th - January 3rd';
export const TWEET_TEXT = META_DESCRIPTION;
export const COOKIE = 'user-id';

// Remove process.env.NEXT_PUBLIC_... below and replace them with
// strings containing your own privacy policy URL and copyright holder name
export const LEGAL_URL = process.env.NEXT_PUBLIC_PRIVACY_POLICY_URL;
export const COPYRIGHT_HOLDER = process.env.NEXT_PUBLIC_COPYRIGHT_HOLDER;

export const CODE_OF_CONDUCT =
  'https://www.notion.so/vercel/Code-of-Conduct-Example-7ddd8d0e9c354bb597a0faed87310a78';
export const REPO = 'https://github.com/vercel/virtual-event-starter-kit';
export const SAMPLE_TICKET_NUMBER = 1234;
export const NAVIGATION = [
  {
    name: 'Main Stage',
    route: '/stage/a'
  },
  {
    name: 'Lifelong Mission',
    route: '/stage/c'
  },
  {
    name: 'Chaplains',
    route: '/stage/m'
  },
  {
    name: 'Alumni',
    route: '/stage/e'
  },
  {
    name: 'Schedule',
    route: '/schedule'
  },
  {
    name: 'Speakers',
    route: '/speakers'
  },
  {
    name: 'Sponsors',
    route: '/expo'
  }
];

export type TicketGenerationState = 'default' | 'loading';
